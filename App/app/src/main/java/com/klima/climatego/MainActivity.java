package com.klima.climatego;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private TextView tw;
    private ImageButton ib;
    private Button bt_btn;
    private final int REQUEST_BT_ACTION = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConstraintLayout constraintLayout = findViewById(R.id.layout);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();
        Context context = getApplicationContext();
        // CharSequence text = "Bluetooth ist aus"; <--- sowas macht man über die strings.xml und dann mit R.strings.<id> :)
        // CharSequence text2 = "Bluetooth ist an";
        int duration = Toast.LENGTH_SHORT;

        ib = (ImageButton) findViewById(R.id.ib);
        tw = (TextView) findViewById(R.id.head);
        bt_btn = (Button) findViewById(R.id.btn_enable_bluetooth);

        final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                // Bluetooth is not enabled :)

                //handle the "enable bluetooth button"
                bt_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_BT_ACTION);
                    }
                });

                ib.setImageResource(R.drawable.climate_bg_gray);
                // Toast toast = Toast.makeText(context, text, duration);
                // toast.show();
            } else {
                bt_btn.setVisibility(View.INVISIBLE);

                // Toast toast = Toast.makeText(context, text2, duration);
                // toast.show();
                final MainActivity instance = this;
                ib.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(instance, ConnectActivity.class);
                        startActivity(intent);
                    }
                });
            }
        }
        final MainActivity instance = this;
        Animation myanim = (Animation) AnimationUtils.loadAnimation(instance, R.anim.splash);
        ib.startAnimation(myanim);
        tw.startAnimation(myanim);
    }

    //this gets called when the request bluetooth access dialog returns
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_BT_ACTION) {

            //if the user selects allow
            if (resultCode == Activity.RESULT_OK) {
                bt_btn.setVisibility(View.INVISIBLE);
                ImageButton ib = (ImageButton) findViewById(R.id.ib);
                ib.setImageResource(R.drawable.climate_bg);

                // CharSequence text2 = "Bluetooth ist an";
                // Toast toast = Toast.makeText(getApplicationContext(), text2, Toast.LENGTH_SHORT);
                // toast.show();
                final MainActivity instance = this;
                ib.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(instance, ConnectActivity.class);
                        startActivity(intent);
                    }
                });
                // if the user selects deny
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), R.string.alert_after_bt_denial, Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

}



