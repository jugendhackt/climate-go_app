package com.klima.climatego;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;


public class ConnectActivity extends AppCompatActivity {
    private final static String TAG = ConnectActivity.class.getSimpleName();

    // Stops scanning after 20 seconds.
    private final UUID CHARACTERISTIC_UUID = UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b26a8");
    private final UUID SERVICE_UUID = UUID.fromString("4fafc201-1fb5-459e-8fcc-c5c9c331914b");
    private final UUID DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private TextView valueDisplay;


    //This is called for scanning
    private final ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            // We scan with report delay > 0. This will never be called.
        }

        //scan results are handeled here
        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            if (!results.isEmpty()) {
                ScanResult result = results.get(0);
                BluetoothDevice device = result.getDevice();
                String deviceAddress = device.getAddress();

                // Device detected, we can automatically connect to it and stop the scan
                Log.d(TAG, "onBatchScanResults: Device " + deviceAddress + " (" + device.getName() + ") found.");

                device.connectGatt(getApplicationContext(), true, mGattCallback);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e(TAG, "onScanFailed: BLScan error " + Integer.toString(errorCode), new Exception());
        }
    };


    //callbacks after the connection
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d(TAG, "onConnectionStateChange: Connected!");
                gatt.discoverServices();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            // Get the counter characteristic
            BluetoothGattCharacteristic characteristic = gatt
                    .getService(SERVICE_UUID)
                    .getCharacteristic(CHARACTERISTIC_UUID);

            gatt.setCharacteristicNotification(characteristic, true);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            if (DESCRIPTOR_UUID.equals(descriptor.getUuid())) {
                BluetoothGattCharacteristic characteristic = gatt
                        .getService(SERVICE_UUID)
                        .getCharacteristic(CHARACTERISTIC_UUID);
                gatt.readCharacteristic(characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            gatt.readCharacteristic(characteristic);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if (CHARACTERISTIC_UUID.equals(characteristic.getUuid())) {
                //TODO WRONG DTYPE
                final int data = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 0);
                Log.d(TAG, "onCharacteristicRead: " + data);

                valueDisplay.post(new Runnable() {
                    @Override
                    public void run() {
                        valueDisplay.setText(Integer.toString(data));
                    }
                });
            }
        }
    };


// für Huawei: *#*#2846579#*#*

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // The BluetoothAdapter is required for any and all Bluetooth activity.
        mBluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        setContentView(R.layout.activity_connect);
        BluetoothLeScannerCompat leScanner = BluetoothLeScannerCompat.getScanner();
        valueDisplay = (TextView) findViewById(R.id.fsdaten);


        ScanSettings settings = new ScanSettings.Builder()
                .setLegacy(false)
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setReportDelay(1000)
                .setUseHardwareBatchingIfSupported(true)
                .build();

        ScanFilter scanFilter = new ScanFilter.Builder()
                .setServiceUuid(new ParcelUuid(SERVICE_UUID)).build();

        Log.d(TAG, "onCreate: Starting scan");

        //start scan and automatically connect
        leScanner.startScan(Arrays.asList(scanFilter), settings, mScanCallback);

    }

}


